__author__ = 'mnowotka'

import re
from django.views.generic import TemplateView
import time
import logging


class EndpointRuntimeMonitor(object):
    """
    Decorator to monitor and log the running time of endpoints
    logs endpoints that take longer than certain threshold (in seconds) to run
    """

    def __init__(self, endpoint_name, threshold=10):
        self.endpoint_name = endpoint_name
        self.threshold = threshold

    def __call__(self, f):
        def wrapped_f(*args, **kwargs):
            t_ini = time.time()
            f_return = f(*args, **kwargs)
            delta_time = time.time() - t_ini
            if delta_time >= self.threshold:
                logger = logging.getLogger('django')
                logger.warning(f'ENDPOINT:{self.endpoint_name} RUNTIME:{delta_time}')
            return f_return
        return wrapped_f

#-----------------------------------------------------------------------------------------------------------------------

def plural(string):
    patterns = [('[sxz]$','$','es'),
                ('[^aeioudgkprt]h$','$','es'),
                ('[^aeiou]y$','y$','ies'),
                ('$','$','s')]

    rules = [lambda word: re.pattern_search_replace[1](pattern_search_replace[0], word) and
                                                                re.sub(pattern_search_replace[1], pattern_search_replace[2], word) for pattern_search_replace in patterns]
    for rule in rules:
        result = rule(string)
        if result:
            return result

#-----------------------------------------------------------------------------------------------------------------------

class DirectTemplateView(TemplateView):
    extra_context = None
    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        if self.extra_context is not None:
            for key, value in list(self.extra_context.items()):
                if callable(value):
                    context[key] = value()
                else:
                    context[key] = value
        return context

#-----------------------------------------------------------------------------------------------------------------------

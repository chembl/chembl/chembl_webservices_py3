import time
import pickle
from rdkit import Chem
from chembl_structure_pipeline import standardizer
from chembl_core_db.utils import EndpointRuntimeMonitor

# Variable loaded from the Settings to prevent circular references
SSS_RDKIT_FILE_PATH = None
SSS_RDKIT_LIB = None


def get_rdkit_slib():
    global SSS_RDKIT_FILE_PATH, SSS_RDKIT_LIB
    if SSS_RDKIT_LIB is None:
        t_ini = time.time()
        print('LOADING RDKIT SSSLIB . . .')
        with open(SSS_RDKIT_FILE_PATH, "rb") as inf:
            SSS_RDKIT_LIB = pickle.load(inf)
            print('RDKIT SUBSTRUCTURE FILE LOADED IN {0} SECS'.format(time.time()-t_ini))
    return SSS_RDKIT_LIB


@EndpointRuntimeMonitor('SUBSTRUCTURE-GET')
def get_substructure_molregnos(query_string):
    mol_query = Chem.MolFromMolBlock(query_string)
    if not mol_query:
        mol_query = Chem.MolFromSmiles(query_string)
        if not mol_query:
            raise Exception("Problem standardizing the query molecule")
    mol_query = standardizer.standardize_mol(mol_query)
    slib = get_rdkit_slib()
    mids = slib.GetMatches(mol_query, maxResults=30000, numThreads=4)
    molregnos = [slib.GetKeyHolder().GetKey(mid) for mid in mids]
    return molregnos

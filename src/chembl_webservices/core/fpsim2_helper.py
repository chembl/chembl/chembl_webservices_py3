from FPSim2 import FPSim2Engine
import time

from FPSim2 import FPSim2Engine
from rdkit import Chem
from chembl_structure_pipeline import standardizer
from chembl_core_db.utils import EndpointRuntimeMonitor

# Variable loaded from the Settings to prevent circular references
FPSIM2_SIM_SQL_URL = None
FPSIM2_SIM_SQL_TABLE = None
FPSIM_ENGINE = None


def get_fpsim_engine():
    global FPSIM_ENGINE, FPSIM2_SIM_SQL_URL, FPSIM2_SIM_SQL_TABLE
    if FPSIM_ENGINE is None:
        t_ini = time.time()
        FPSIM_ENGINE = FPSim2Engine(
            conn_url=FPSIM2_SIM_SQL_URL, table_name=FPSIM2_SIM_SQL_TABLE, storage_backend="sqla"
        )
        print('FPSIM2 FILE LOADED IN {0} SECS'.format(time.time()-t_ini))
    return FPSIM_ENGINE


@EndpointRuntimeMonitor('FPSIM2-GET')
def get_similar_molregnos(query_string, similarity=0.7):
    """
    :param query_string: the smiles, inchi or molfile representation of the query
    :param similarity: the minimum similarity threshold
    :return: a list with tuples of (molregno, similarity)
    """
    mol_query = Chem.MolFromMolBlock(query_string)
    if not mol_query:
        mol_query = Chem.MolFromSmiles(query_string)
        if not mol_query:
            raise Exception("Problem standardizing the query molecule")
    mol_query = standardizer.standardize_mol(mol_query)
    query_string_std = Chem.MolToMolBlock(mol_query)
    if similarity < 0.4 or similarity > 1:
        raise ValueError('Similarity should have a value between 0.4 and 1.')

    return get_fpsim_engine().similarity(query_string_std, similarity, n_workers=1)

__author__ = 'mnowotka'

from tastypie.utils import trailing_slash
from django.conf.urls import url
from chembl_webservices import __version__
from chembl_webservices.core.resource import ChemblModelResource
from chembl_webservices.core.meta import ChemblResourceMeta
from chembl_webservices.core.serialization import ChEMBLApiSerializer

from chembl_core_model.models import Version
from chembl_core_model.models import TargetDictionary
from chembl_core_model.models import CompoundRecords
from chembl_core_model.models import MoleculeDictionary
from chembl_core_model.models import Activities
from chembl_core_model.models import Docs

import re
from chembl_webservices.core.fields import monkeypatch_tastypie_field
monkeypatch_tastypie_field()

# ----------------------------------------------------------------------------------------------------------------------


class StatusResource(ChemblModelResource):

# ----------------------------------------------------------------------------------------------------------------------

    class Meta(ChemblResourceMeta):
        resource_name = 'status'
        serializer = ChEMBLApiSerializer(resource_name)
        # This line is required to prevent Django ImproperlyConfigured: ModelResource
        object_class = None

# ----------------------------------------------------------------------------------------------------------------------

    def base_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash(),),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/datatables\.(?P<format>\w+)$" % self._meta.resource_name,
                self.wrap_view('get_datatables'), name="api_get_datatables"),
            url(r"^(?P<resource_name>%s)%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('dispatch_detail'), name="api_dispatch_list"),
            url(r"^(?P<resource_name>%s)\.(?P<format>\w+)%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('dispatch_detail'), name="api_dispatch_list"),
        ]

# ----------------------------------------------------------------------------------------------------------------------

    def prepend_urls(self):
        return []

# ----------------------------------------------------------------------------------------------------------------------

    def get_detail(self, request, **kwargs):
        versions = Version.objects.all()
        chembl_version = None
        for v_i in versions:
            if re.fullmatch(r'chembl_\d+', v_i.name, re.IGNORECASE):
                chembl_version = v_i
                break
        chembl_version_name = 'UNKNOWN - REGEX (chembl_\\d+) FAILED'
        chembl_version_date = 'UNKNOWN - REGEX (chembl_\\d+) FAILED'
        if chembl_version:
            chembl_version_name = chembl_version.name
            chembl_version_date = chembl_version.creation_date
        return self.create_response(request,
                                    {
                                        'status': 'UP',
                                        'chembl_db_version': chembl_version_name,
                                        'chembl_release_date': chembl_version_date,
                                        'targets': TargetDictionary.objects.all().count(),
                                        'compound_records': CompoundRecords.objects.all().count(),
                                        'disinct_compounds': MoleculeDictionary.objects.all().count(),
                                        'activities': Activities.objects.all().count(),
                                        'publications': Docs.objects.all().count(),
                                    }
                                    )

# ----------------------------------------------------------------------------------------------------------------------

    def get_schema(self, request, **kwargs):
        return self.create_response(request, {
            'allowed_detail_http_methods': ['get'],
            'allowed_list_http_methods': ['get']
        })

# ----------------------------------------------------------------------------------------------------------------------

    def get_datatables(self, request, **kwargs):
        return self.create_response(request, {
            "columns": [
                {
                    "title": "status",
                    "data": "status",
                    "orderable": False
                },
                {
                    "title": "version",
                    "data": "version",
                    "orderable": False
                }
            ]
        })

# ----------------------------------------------------------------------------------------------------------------------

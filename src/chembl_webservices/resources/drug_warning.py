__author__ = 'jfmosquera'

from tastypie import fields
from chembl_webservices.core.utils import NUMBER_FILTERS, CHAR_FILTERS
from tastypie.resources import ALL, ALL_WITH_RELATIONS
from chembl_webservices.core.resource import ChemblModelResource
from chembl_webservices.core.meta import ChemblResourceMeta
from chembl_webservices.core.serialization import ChEMBLApiSerializer
from django.db.models import Prefetch
from chembl_core_model.models import DrugWarning
from chembl_core_model.models import WarningRefs
from chembl_core_model.models import MoleculeDictionary

from chembl_webservices.core.fields import monkeypatch_tastypie_field

monkeypatch_tastypie_field()


# ----------------------------------------------------------------------------------------------------------------------


class WarningRefsResource(ChemblModelResource):
    class Meta(ChemblResourceMeta):
        queryset = WarningRefs.objects.all()
        excludes = []
        resource_name = 'warning_ref'
        collection_name = 'warning_refs'
        detail_uri_name = 'warnref_id'
        serializer = ChEMBLApiSerializer(resource_name, {collection_name: resource_name})
        prefetch_related = []

        fields = (
            'ref_type',
            'ref_id',
            'ref_url',
        )

        filtering = {
            'ref_type': CHAR_FILTERS,
            'ref_id': CHAR_FILTERS,
            'ref_url': CHAR_FILTERS,
        }

        ordering = [field for field in list(filtering.keys()) if not ('comment' in field or 'description' in field)]


# ----------------------------------------------------------------------------------------------------------------------


class DrugWarningResource(ChemblModelResource):
    molecule_chembl_id = fields.CharField('molecule__chembl_id', null=True, blank=True)
    warning_refs = fields.ToManyField('chembl_webservices.resources.drug_warning.WarningRefsResource',
                                         'warningrefs_set', full=True, null=True, blank=True)
    parent_molecule_chembl_id = fields.CharField('molecule__moleculehierarchy__parent_molecule__chembl_id', null=True,
                                                 blank=True)

    class Meta(ChemblResourceMeta):
        queryset = DrugWarning.objects.all()
        excludes = []
        resource_name = 'drug_warning'
        collection_name = 'drug_warnings'
        detail_uri_name = 'warning_id'
        serializer = ChEMBLApiSerializer(resource_name, {collection_name: resource_name})
        prefetch_related = [
            Prefetch('molecule', queryset=MoleculeDictionary.objects.only('chembl')),
            'warningrefs_set',
            Prefetch('molecule__moleculehierarchy__parent_molecule',
                     queryset=MoleculeDictionary.objects.only('chembl')),
        ]

        fields = (
            'warning_id',
            'molecule_chembl_id',
            'parent_molecule_chembl_id',
            'warning_type',
            'warning_class',
            'warning_description',
            'warning_country',
            'warning_year',
            'warning_refs',
            'efo_term',
            'efo_id',
            'efo_id_for_warning_class',
        )

        filtering = {
            'warning_id': NUMBER_FILTERS,
            'molecule_chembl_id': ALL,
            'parent_molecule_chembl_id': ALL,
            'warning_type': CHAR_FILTERS,
            'warning_class': CHAR_FILTERS,
            'warning_description': CHAR_FILTERS,
            'warning_country': CHAR_FILTERS,
            'warning_year': NUMBER_FILTERS,
            'warning_refs': ALL_WITH_RELATIONS,
            'efo_term': CHAR_FILTERS,
            'efo_id': CHAR_FILTERS,
            'efo_id_for_warning_class': CHAR_FILTERS,
        }

        ordering = [field for field in list(filtering.keys()) if not ('comment' in field or 'description' in field)]

# ----------------------------------------------------------------------------------------------------------------------

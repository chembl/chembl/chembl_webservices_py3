
from tastypie.resources import ALL
from chembl_webservices.core.resource import ChemblModelResource
from chembl_webservices.core.meta import ChemblResourceMeta
from chembl_core_model.models import VariantSequences

# ----------------------------------------------------------------------------------------------------------------------


class VariantSequenceResource(ChemblModelResource):

    class Meta(ChemblResourceMeta):
        queryset = VariantSequences.objects.all()
        excludes = ['variant_id']
        resource_name = 'variant_sequence'

        filtering = {
            'variant_id': ALL,
            'mutation': ALL,
            'accession': ALL,
            'version': ALL,
            'isoform': ALL,
            'sequence': ALL,
            'organism': ALL,
            'tax_id': ALL,
        }

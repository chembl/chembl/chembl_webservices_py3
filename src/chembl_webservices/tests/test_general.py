from chembl_webservices.tests import BaseWebServiceTestCase


class GeneralTestCase(BaseWebServiceTestCase):

    def test_status(self):
        status_res = self.request_url(self.WS_URL + '/status.json')
        message = 'Invalid status response!'
        self.assertEqual(status_res['activities'], 17276334, message)
        self.assertEqual(status_res['chembl_db_version'], 'ChEMBL_28', message)
        self.assertEqual(status_res['chembl_release_date'], '2021-01-15T00:00:00', message)
        self.assertEqual(status_res['compound_records'], 2680904, message)
        self.assertEqual(status_res['disinct_compounds'], 2086898, message)
        self.assertEqual(status_res['publications'], 80480, message)
        self.assertEqual(status_res['status'], 'UP', message)
        self.assertEqual(status_res['targets'], 14347, message)

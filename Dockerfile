FROM python:3.9 AS base

ARG USERNAME=${USERNAME:-chembl_ws_user}
ARG GROUPNAME=${GROUPNAME:-chembl_ws_group}
ARG UID=${UID:-123}
ARG GID=${GID:-321}
ARG WORKDIR=${WORKDIR:-/chembl_ws_py3}

# setup user and app root directory
RUN useradd -m ${USERNAME} -u ${UID}
RUN mkdir -p ${WORKDIR}
RUN chown -R ${UID}:${GID} ${WORKDIR}
WORKDIR ${WORKDIR}

# setup gunicorn log and pid folder
RUN mkdir ${WORKDIR}/gunicorn
RUN chown -R ${UID}:${GID} ${WORKDIR}/gunicorn

# setup data folder
RUN mkdir ${WORKDIR}/data
RUN chown -R ${UID}:${GID} ${WORKDIR}/data

# prevents Python from writing pyc files to disc and from buffering stdout and stderr
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install pip requirements
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

FROM base AS ws_source
# copy webservices code and config files
COPY src src
COPY manage.py manage.py
ENTRYPOINT [ "python", "manage.py", "runserver", "0.0.0.0:3000" ]

FROM ws_source AS ws_gunicorn_server
# copy gunicorn files
COPY gunicorn.conf.py gunicorn.conf.py
ENTRYPOINT [ "gunicorn", "-c", "gunicorn.conf.py", "chembl_ws_app.wsgi:application" ]
